import Vue from 'vue'
import Router from 'vue-router'
import Comunnication from '@/pages/Comunnication.vue'
import Instagram from '@/pages/Instagram.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Comunnication',
      component: Comunnication
    },
    {
      path: '/Instagram',
      name: 'Instagram',
      component: Instagram
    },
    {
      path: '/Comunnication',
      name: 'Comunnication',
      component: Comunnication
    }
  ]
})
